package com.krungsri.basicweatherforecast.di

import com.krungsri.basicweatherforecast.data.api.base.BaseOkHttpClientBuilder
import com.krungsri.basicweatherforecast.data.api.base.BaseRetrofitBuilder
import com.krungsri.basicweatherforecast.data.api.interceptor.AuthInterceptor
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    single { BaseOkHttpClientBuilder(get()) }

    single<CallAdapter.Factory> { RxJava2CallAdapterFactory.create() }

    single<Converter.Factory> { GsonConverterFactory.create() }

    single { BaseRetrofitBuilder(get(), get(), get()) }

    factory { AuthInterceptor() }
}