package com.krungsri.basicweatherforecast.di

import com.krungsri.basicweatherforecast.data.api.WeatherApi
import com.krungsri.basicweatherforecast.data.api.base.BaseRetrofitBuilder
import com.krungsri.basicweatherforecast.data.datasource.WeatherRepository
import com.krungsri.basicweatherforecast.data.datasource.WeatherRepositoryImpl
import com.krungsri.basicweatherforecast.presentation.current.CurrentWeatherViewModel
import com.krungsri.basicweatherforecast.presentation.forecast.WeatherForecastViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val weatherModule = module {

    single<WeatherApi> { get<BaseRetrofitBuilder>().build() }

    factory<WeatherRepository> { WeatherRepositoryImpl(get()) }

    viewModel { CurrentWeatherViewModel(get()) }
    viewModel { WeatherForecastViewModel(get()) }
}