package com.krungsri.basicweatherforecast.presentation.forecast.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.krungsri.basicweatherforecast.R
import com.krungsri.basicweatherforecast.data.entity.WeatherForecastData
import com.krungsri.basicweatherforecast.presentation.forecast.adapter.holder.ContentWeatherForecastHolder
import com.krungsri.basicweatherforecast.presentation.forecast.adapter.holder.HeaderWeatherForecastHolder
import org.zakariya.stickyheaders.SectioningAdapter

class WeatherForecastAdapter : SectioningAdapter() {

    var weatherForecastDataList: WeatherForecastData? = null

    override fun getNumberOfSections(): Int {
        return weatherForecastDataList?.headerWeatherForecastData?.size ?: 0
    }

    override fun getNumberOfItemsInSection(sectionIndex: Int): Int {
        return weatherForecastDataList?.let {
            it.headerWeatherForecastData[sectionIndex].contentWeatherList.size
        } ?: 0
    }

    override fun doesSectionHaveHeader(sectionIndex: Int): Boolean {
        return true
    }

    override fun doesSectionHaveFooter(sectionIndex: Int): Boolean {
        return false
    }

    override fun onCreateItemViewHolder(parent: ViewGroup, itemUserType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_content_weather_forecast, parent, false)
        return ContentWeatherForecastHolder((view))
    }

    override fun onCreateHeaderViewHolder(
        parent: ViewGroup,
        headerUserType: Int
    ): HeaderViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_header_weather_forecast, parent, false)
        return HeaderWeatherForecastHolder(view)
    }

    override fun onBindItemViewHolder(
        viewHolder: ItemViewHolder?,
        sectionIndex: Int,
        itemIndex: Int,
        itemUserType: Int
    ) {
        super.onBindItemViewHolder(viewHolder, sectionIndex, itemIndex, itemUserType)
        if (viewHolder is ContentWeatherForecastHolder) {
            weatherForecastDataList?.also {
                val header = it.headerWeatherForecastData[sectionIndex]
                viewHolder.bind(header.contentWeatherList[itemIndex])
            }
        }
    }

    override fun onBindHeaderViewHolder(
        viewHolder: HeaderViewHolder?,
        sectionIndex: Int,
        headerUserType: Int
    ) {
        super.onBindHeaderViewHolder(viewHolder, sectionIndex, headerUserType)
        if (viewHolder is HeaderWeatherForecastHolder) {
            weatherForecastDataList?.headerWeatherForecastData?.also {
                viewHolder.bind(it[sectionIndex])
            }
        }
    }
}