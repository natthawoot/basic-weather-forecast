package com.krungsri.basicweatherforecast.presentation.base

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.toolbar_with_title.*

abstract class BaseFragment : Fragment() {

    open val TAG: String? = null

    private var toolbar: Toolbar? = null
    private var toolbarTitleTv: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity?.finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun initToolBar(
        toolbarId: Int = 0,
        toolbarTitleId: Int = 0,
        toolbarBackId: Int = 0
    ) {
        if (toolbarId > 0) {
            toolbar = view?.findViewById(toolbarId)
            (activity as BaseActivity).setSupportActionBar(toolbar)
        }

        if (toolbarTitleId > 0) {
            toolbarTitleTv = view?.findViewById(toolbarTitleId)
        }

        if (toolbarBackId > 0) {
            view?.findViewById<View>(toolbarBackId)?.setOnClickListener { onToolbarBackPressed() }
        }
    }

    fun setToolbarTitle(title: String) {
        toolbarTitleTv?.also {
            it.text = title
        }
    }

    fun showToolbarBack(isShow: Boolean) {
        if (isShow) {
            toolbarBackButton.visibility = View.VISIBLE
        } else {
            toolbarBackButton.visibility = View.GONE
        }
    }

    open fun onToolbarActionClick(view: View) {}

    private fun onToolbarBackPressed() {
        activity?.onBackPressed()
    }

}