package com.krungsri.basicweatherforecast.presentation.base

import android.view.View
import android.widget.TextView
import androidx.annotation.AnimRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentManager
import kotlinx.android.synthetic.main.toolbar_with_title.*

abstract class BaseActivity : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    private var toolbarTitleTv: TextView? = null
    open var containerResId: Int = 0

    open fun onToolbarActionClick(view: View) {}

    private fun onToolbarBackPressed() {
        onBackPressed()
    }

    fun initToolBar(
        toolbarId: Int = 0,
        toolbarTitleId: Int = 0,
        toolbarBackId: Int = 0
    ) {
        if (toolbarId > 0) {
            toolbar = findViewById(toolbarId)
            setSupportActionBar(toolbar)
        }

        if (toolbarTitleId > 0) {
            toolbarTitleTv = findViewById(toolbarTitleId)
        }

        if (toolbarBackId > 0) {
            findViewById<View>(toolbarBackId).setOnClickListener { onToolbarBackPressed() }
        }
    }

    fun hideToolBar() {
        toolbar?.visibility = View.GONE
    }

    fun showToolbar() {
        toolbar?.visibility = View.VISIBLE
    }

    fun setToolbarTitle(title: String) {
        if (toolbarTitleTv != null) {
            toolbarTitleTv!!.text = title
        } else {
            supportActionBar!!.title = title
        }
    }

    fun showToolbarBack(isShow: Boolean) {
        if (isShow) {
            toolbarBackButton.visibility = View.VISIBLE
        } else {
            toolbarBackButton.visibility = View.GONE
        }
    }

    fun replaceFragmentAndClear(fragment: BaseFragment) {
        if (!isFinishing) {
            clearBackStack()
            replaceFragmentAllowStateLoss(fragment, false)
        }
    }

    fun replaceFragment(
        fragment: BaseFragment,
        addToBackStack: Boolean = false,
        containerId: Int = containerResId,
        tag: String? = fragment.TAG
    ) {
        if (!isFinishing) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(containerId, fragment, tag)
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(null)
            }
            fragmentTransaction.commit()
        }
    }

    fun replaceFragmentAllowStateLoss(
        fragment: BaseFragment,
        addToBackStack: Boolean,
        containerId: Int = containerResId
    ) {
        if (!isFinishing) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(containerId, fragment, fragment.TAG)
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(null)
            }
            fragmentTransaction.commitAllowingStateLoss()
        }
    }

    fun addFragment(
        fragment: BaseFragment,
        addToBackStack: Boolean,
        containerId: Int = containerResId
    ) {
        if (!isFinishing) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.add(containerId, fragment, fragment.TAG)
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(null)
            }
            fragmentTransaction.commit()
        }
    }

    fun addFragmentWithAnim(
        containerId: Int, fragment: BaseFragment, @AnimRes enter: Int,
        @AnimRes exit: Int, @AnimRes popEnter: Int, @AnimRes popExit: Int,
        addToBackStack: Boolean
    ) {
        if (!isFinishing) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(enter, exit, popEnter, popExit)
            fragmentTransaction.add(containerId, fragment, fragment.TAG)
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(null)
            }
            fragmentTransaction.commit()
        }
    }

    private fun clearBackStack() {
        try {
            supportFragmentManager.run {
                if (backStackEntryCount > 0) {
                    val first = getBackStackEntryAt(0)
                    popBackStack(first.id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                }
            }
        } catch (e: IllegalStateException) {
        }
    }
}
