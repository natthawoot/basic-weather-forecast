package com.krungsri.basicweatherforecast.presentation.forecast.adapter.holder

import android.view.View
import com.krungsri.basicweatherforecast.R
import com.krungsri.basicweatherforecast.data.entity.HeaderWeatherForecastData
import com.krungsri.basicweatherforecast.extension.getUrlImage
import com.krungsri.basicweatherforecast.extension.loadImage
import kotlinx.android.synthetic.main.item_header_weather_forecast.view.*
import org.zakariya.stickyheaders.SectioningAdapter

class HeaderWeatherForecastHolder(itemView: View) : SectioningAdapter.HeaderViewHolder(itemView) {

    fun bind(headerWeatherForecast: HeaderWeatherForecastData) {
        val context = itemView.context
        itemView.itemHeaderWeatherForecast_icon_imageView.loadImage(headerWeatherForecast.icon.getUrlImage())
        itemView.itemHeaderWeatherForecast_temp_textView.text =
            context.getString(R.string.currentTemp, headerWeatherForecast.temp)
        itemView.itemHeaderWeatherForecast_place_textView.text = headerWeatherForecast.place
    }

}