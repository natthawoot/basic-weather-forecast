package com.krungsri.basicweatherforecast.presentation.current

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.Observer
import com.krungsri.basicweatherforecast.R
import com.krungsri.basicweatherforecast.data.api.entity.response.CurrentWeatherResponse
import com.krungsri.basicweatherforecast.extension.format
import com.krungsri.basicweatherforecast.extension.getUrlImage
import com.krungsri.basicweatherforecast.extension.loadImage
import com.krungsri.basicweatherforecast.extension.toDate
import com.krungsri.basicweatherforecast.presentation.base.BaseActivity
import com.krungsri.basicweatherforecast.presentation.base.BaseFragment
import com.krungsri.basicweatherforecast.presentation.forecast.WeatherForecastFragment
import com.krungsri.basicweatherforecast.state.LoadingState
import com.krungsri.basicweatherforecast.state.ValidateState
import kotlinx.android.synthetic.main.fragment_current_weather.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CurrentWeatherFragment : BaseFragment() {

    private val viewModel: CurrentWeatherViewModel by viewModel()

    private var isFahrenheit: Boolean = false
    private var lat: Double = 0.0
    private var lon: Double = 0.0
    private var place: String = ""

    override val TAG = "CurrentWeatherFragment"

    companion object {

        fun newInstance() = CurrentWeatherFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_current_weather, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeViewModel()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.cleanUp()
    }

    private fun initView() {
        initToolBar(toolbarId = R.id.toolbar, toolbarTitleId = R.id.toolbarTitle)
        setToolbarTitle(getString(R.string.title_currentWeather))
        currentWeather_city_editText.imeOptions = EditorInfo.IME_ACTION_SEARCH
        currentWeather_city_editText.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideKeyboard()
                viewModel.validate(v.text.toString())
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
        currentWeather_forecast_button.setOnClickListener {
            (activity as BaseActivity).replaceFragment(
                WeatherForecastFragment.newInstance(
                    lat,
                    lon,
                    isFahrenheit,
                    place
                ), addToBackStack = true
            )
        }
        currentWeather_unit_toggleButton.setOnCheckedChangeListener { _, isChecked ->
            hideKeyboard()
            isFahrenheit = isChecked
            viewModel.validate(currentWeather_city_editText.text.toString())
        }
    }

    private fun observeViewModel() {
        viewModel.loadingStateMutableLiveData
            .observe(viewLifecycleOwner, Observer {
                when (it.status) {
                    LoadingState.Status.LOADING -> stateLoading()
                    LoadingState.Status.SUCCESS -> stateSuccess()
                    LoadingState.Status.FAILED -> stateError(it.message)
                }
            })
        viewModel.currentWeatherMutableLiveData
            .observe(viewLifecycleOwner, Observer { it ->
                it.coordinate?.also { coordinate ->
                    lat = coordinate.lat
                    lon = coordinate.lon
                }
                setupViewWeather(it)
            })
        viewModel.validateMutableLiveData
            .observe(viewLifecycleOwner, Observer {
                when (it.status) {
                    ValidateState.ValidateStatus.SUCCESS -> viewModel.getCurrentWeather(
                        currentWeather_city_editText.text.toString(),
                        isFahrenheit
                    )
                    ValidateState.ValidateStatus.FAILED -> stateError(getString(R.string.label_error_empty))
                }
            })
    }

    private fun stateLoading() {
        currentWeather_progressbar.visibility = View.VISIBLE
    }

    private fun stateSuccess() {
        currentWeather_progressbar.visibility = View.GONE
        currentWeather_error_textView.visibility = View.GONE
        currentWeather_content_constraintLayout.visibility = View.VISIBLE
    }

    private fun stateError(message: String?) {
        currentWeather_progressbar.visibility = View.GONE
        currentWeather_content_constraintLayout.visibility = View.GONE
        currentWeather_error_textView.visibility = View.VISIBLE
        currentWeather_error_textView.text = message ?: getString(R.string.label_error)
    }

    private fun setupViewWeather(currentWeather: CurrentWeatherResponse) {
        currentWeather_dateTime_textView.text = currentWeather.dateTime.toLong().toDate().format()
        currentWeather.main?.also {
            currentWeather_currentTemp_textView.text = getString(R.string.currentTemp, it.temp)
            currentWeather_otherData_textView.text = getString(
                R.string.otherData, it.tempMin,
                it.tempMax, it.feelsLike
            )
            currentWeather_humidity_textView.text = getString(R.string.humidity, it.humidity)
        }
        currentWeather.sys?.also {
            place = getString(R.string.place, currentWeather.name, it.country)
            currentWeather_city_textView.text = place
            currentWeather_sunrise_textView.text =
                getString(R.string.sunrise, it.sunrise.toLong().toDate().format())
            currentWeather_sunset_textView.text =
                getString(R.string.sunset, it.sunset.toLong().toDate().format())
        }
        if (currentWeather.weather.isNotEmpty()) {
            currentWeather_icon_imageView.loadImage(currentWeather.weather[0].icon.getUrlImage())
            currentWeather_description_textView.text = currentWeather.weather[0].description
        }
    }

    private fun hideKeyboard() {
        activity?.also {
            val inputMethod =
                (it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            inputMethod.hideSoftInputFromWindow(currentWeather_city_editText.windowToken, 0)
        }
    }

}
