package com.krungsri.basicweatherforecast.presentation.forecast

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.krungsri.basicweatherforecast.constants.CELSIUS
import com.krungsri.basicweatherforecast.constants.FAHRENHEIT
import com.krungsri.basicweatherforecast.constants.HOUR_24
import com.krungsri.basicweatherforecast.data.api.entity.response.*
import com.krungsri.basicweatherforecast.data.datasource.WeatherRepository
import com.krungsri.basicweatherforecast.data.entity.ContentWeatherForecastData
import com.krungsri.basicweatherforecast.data.entity.HeaderWeatherForecastData
import com.krungsri.basicweatherforecast.data.entity.WeatherForecastData
import com.krungsri.basicweatherforecast.extension.addTo
import com.krungsri.basicweatherforecast.extension.toObject
import com.krungsri.basicweatherforecast.state.LoadingState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class WeatherForecastViewModel(private val weatherRepository: WeatherRepository) : ViewModel() {

    val loadingStateMutableLiveData: MutableLiveData<LoadingState> = MutableLiveData()
    val weatherForecastMutableLiveData: MutableLiveData<WeatherForecastData> = MutableLiveData()

    private val disposeBag = CompositeDisposable()

    fun getForecast(lat: Double, lon: Double, isFahrenheit: Boolean, place: String) {
        val unit = if (isFahrenheit) {
            FAHRENHEIT
        } else {
            CELSIUS
        }
        weatherRepository.getForecast(lat, lon, unit)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                loadingStateMutableLiveData.value = LoadingState.LOADING
            }
            .subscribe({
                loadingStateMutableLiveData.value = LoadingState.SUCCESS
                weatherForecastMutableLiveData.value = mapData(it, place)
            }, {
                if (it is HttpException) {
                    val error = it.toObject<ErrorResponse>()
                    loadingStateMutableLiveData.value = LoadingState.error(error?.message)
                } else {
                    loadingStateMutableLiveData.value = LoadingState.error(null)
                }
            }).addTo(disposeBag)
    }

    fun cleanUp() {
        disposeBag.clear()
    }

    private fun mapData(
        forecastResponse: ForecastResponse,
        place: String
    ): WeatherForecastData {
        val headerList: MutableList<HeaderWeatherForecastData> = mutableListOf()
        val contentList: MutableList<ContentWeatherForecastData> = mutableListOf()
        // map header
        forecastResponse.currentForecast?.also {
            val header = mapHeader(it, place)
            forecastResponse.hourly.forEachIndexed { index, hourForecast ->
                if (index >= HOUR_24) {
                    return@forEachIndexed
                }
                contentList.add(mapContent(hourForecast))
            }
            header.contentWeatherList = contentList
            headerList.add(header)
        }
        return WeatherForecastData(headerList)
    }

    private fun mapHeader(
        currentForecast: CurrentForecast,
        place: String
    ): HeaderWeatherForecastData {
        currentForecast.let {
            val icon = getIcon(it.weather)
            return HeaderWeatherForecastData(
                icon = icon, temp = it.temp,
                place = place, contentWeatherList = mutableListOf()
            )
        }
    }

    private fun mapContent(hourForecast: HourForecast): ContentWeatherForecastData {
        val icon = getIcon(hourForecast.weather)
        return ContentWeatherForecastData(
            icon,
            hourForecast.temp,
            hourForecast.dateTime,
            hourForecast.humidity,
            hourForecast.feelsLike
        )
    }

    private fun getIcon(weather: List<Weather>): String {
        return if (weather.isNotEmpty()) {
            weather[0].icon
        } else {
            ""
        }
    }
}