package com.krungsri.basicweatherforecast.presentation.forecast.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.krungsri.basicweatherforecast.R
import com.krungsri.basicweatherforecast.data.entity.ContentWeatherForecastData
import com.krungsri.basicweatherforecast.extension.format
import com.krungsri.basicweatherforecast.extension.getUrlImage
import com.krungsri.basicweatherforecast.extension.loadImage
import com.krungsri.basicweatherforecast.extension.toDate
import kotlinx.android.synthetic.main.item_content_weather_forecast.view.*
import org.zakariya.stickyheaders.SectioningAdapter

class ContentWeatherForecastHolder(itemView: View) : SectioningAdapter.ItemViewHolder(itemView) {

    fun bind(contentWeatherForecast: ContentWeatherForecastData) {
        val context = itemView.context
        itemView.itemContentWeatherForecast_icon_imageView.loadImage(contentWeatherForecast.icon.getUrlImage())
        itemView.itemContentWeatherForecast_temp_textView.text =
            context.getString(R.string.currentTemp, contentWeatherForecast.temp)
        itemView.itemContentWeatherForecast_dateTime_textView.text =
            contentWeatherForecast.dateTime.toLong().toDate().format()
        itemView.itemContentWeatherForecast_feelsLike_textView.text = context.getString(
            R.string.feelsLike,
            contentWeatherForecast.feelsLike
        )
        itemView.itemContentWeatherForecast_humidity_textView.text =
            context.getString(R.string.humidity, contentWeatherForecast.humidity)
    }

}