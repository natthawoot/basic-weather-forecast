package com.krungsri.basicweatherforecast.presentation.current

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.krungsri.basicweatherforecast.constants.CELSIUS
import com.krungsri.basicweatherforecast.constants.FAHRENHEIT
import com.krungsri.basicweatherforecast.data.api.entity.response.CurrentWeatherResponse
import com.krungsri.basicweatherforecast.data.api.entity.response.ErrorResponse
import com.krungsri.basicweatherforecast.data.datasource.WeatherRepository
import com.krungsri.basicweatherforecast.extension.addTo
import com.krungsri.basicweatherforecast.extension.isCharacterEngAlphabet
import com.krungsri.basicweatherforecast.extension.toObject
import com.krungsri.basicweatherforecast.state.LoadingState
import com.krungsri.basicweatherforecast.state.ValidateState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class CurrentWeatherViewModel(private val weatherRepository: WeatherRepository) : ViewModel() {

    val loadingStateMutableLiveData: MutableLiveData<LoadingState> = MutableLiveData()
    val currentWeatherMutableLiveData: MutableLiveData<CurrentWeatherResponse> = MutableLiveData()
    val validateMutableLiveData: MutableLiveData<ValidateState> = MutableLiveData()

    private val disposeBag = CompositeDisposable()

    fun validate(city: String) {
        if (city.isBlank() || !city.isCharacterEngAlphabet()) {
            validateMutableLiveData.value = ValidateState.FAILED
        } else {
            validateMutableLiveData.value = ValidateState.SUCCESS
        }
    }

    fun getCurrentWeather(city: String, isFahrenheit: Boolean) {
        val unit = if (isFahrenheit) {
            FAHRENHEIT
        } else {
            CELSIUS
        }
        weatherRepository.getCurrentWeather(city, unit)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                loadingStateMutableLiveData.value = LoadingState.LOADING
            }
            .subscribe({
                loadingStateMutableLiveData.value = LoadingState.SUCCESS
                currentWeatherMutableLiveData.value = it
            }, {
                if (it is HttpException) {
                    val error = it.toObject<ErrorResponse>()
                    loadingStateMutableLiveData.value = LoadingState.error(error?.message)
                } else {
                    loadingStateMutableLiveData.value = LoadingState.error(null)
                }
            }).addTo(disposeBag)
    }

    fun cleanUp() {
        disposeBag.clear()
    }

}