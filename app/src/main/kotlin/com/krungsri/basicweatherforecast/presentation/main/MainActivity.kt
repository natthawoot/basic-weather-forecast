package com.krungsri.basicweatherforecast.presentation.main

import android.os.Bundle
import com.krungsri.basicweatherforecast.R
import com.krungsri.basicweatherforecast.presentation.base.BaseActivity
import com.krungsri.basicweatherforecast.presentation.current.CurrentWeatherFragment

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        containerResId = R.id.main_content_frameLayout
        replaceFragment(CurrentWeatherFragment.newInstance())
    }
}
