package com.krungsri.basicweatherforecast.presentation.forecast

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.krungsri.basicweatherforecast.R
import com.krungsri.basicweatherforecast.presentation.base.BaseFragment
import com.krungsri.basicweatherforecast.presentation.forecast.adapter.WeatherForecastAdapter
import com.krungsri.basicweatherforecast.state.LoadingState
import kotlinx.android.synthetic.main.fragment_weather_forecast.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.zakariya.stickyheaders.StickyHeaderLayoutManager

class WeatherForecastFragment : BaseFragment() {

    private val viewModel: WeatherForecastViewModel by viewModel()

    private lateinit var weatherForecastAdapter: WeatherForecastAdapter

    private var lat: Double = 0.0
    private var lon: Double = 0.0
    private var isFahrenheit: Boolean = false
    private var place: String = ""

    override val TAG = "WeatherForecastFragment"

    companion object {

        private const val ARGUMENT_LAT = "lat"
        private const val ARGUMENT_LON = "lon"
        private const val ARGUMENT_PLACE = "place"
        private const val ARGUMENT_IS_FAHRENHEIT = "isFahrenheit"

        fun newInstance(
            lat: Double,
            lon: Double,
            isFahrenheit: Boolean,
            place: String
        ): WeatherForecastFragment {
            val fragment = WeatherForecastFragment()
            val bundle = Bundle().apply {
                putDouble(ARGUMENT_LAT, lat)
                putDouble(ARGUMENT_LON, lon)
                putBoolean(ARGUMENT_IS_FAHRENHEIT, isFahrenheit)
                putString(ARGUMENT_PLACE, place)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.also {
            lat = it.getDouble(ARGUMENT_LAT)
            lon = it.getDouble(ARGUMENT_LON)
            isFahrenheit = it.getBoolean(ARGUMENT_IS_FAHRENHEIT, false)
            place = it.getString(ARGUMENT_PLACE, "")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_weather_forecast, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observerViewModel()
        viewModel.getForecast(lat, lon, isFahrenheit, place)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.cleanUp()
    }

    private fun initView() {
        initToolBar(
            toolbarId = R.id.toolbar,
            toolbarTitleId = R.id.toolbarTitle,
            toolbarBackId = R.id.toolbarBackButton
        )
        setToolbarTitle(getString(R.string.title_weatherForecast))
        showToolbarBack(isShow = true)
        initAdapter()
    }

    private fun initAdapter() {
        weatherForecastAdapter = WeatherForecastAdapter()
        weatherForecast_content_recyclerView.apply {
            layoutManager = StickyHeaderLayoutManager()
            adapter = weatherForecastAdapter
            addItemDecoration(DividerItemDecoration(
                context,
                LinearLayoutManager.VERTICAL
            ))
        }
    }

    private fun observerViewModel() {
        viewModel.weatherForecastMutableLiveData
            .observe(viewLifecycleOwner, Observer {
                weatherForecastAdapter.weatherForecastDataList = it
                weatherForecastAdapter.notifyDataSetChanged()
            })
        viewModel.loadingStateMutableLiveData
            .observe(viewLifecycleOwner, Observer {
                when (it.status) {
                    LoadingState.Status.LOADING -> stateLoading()
                    LoadingState.Status.SUCCESS -> stateSuccess()
                    LoadingState.Status.FAILED -> stateError(it.message)
                }
            })
    }

    private fun stateLoading() {
        weatherForecast_progressBar.visibility = View.VISIBLE
    }

    private fun stateSuccess() {
        weatherForecast_progressBar.visibility = View.GONE
        weatherForecast_content_recyclerView.visibility = View.VISIBLE
    }

    private fun stateError(message: String?) {
        weatherForecast_progressBar.visibility = View.GONE
        weatherForecast_error_textView.visibility = View.VISIBLE
        weatherForecast_error_textView.text = message
    }
}