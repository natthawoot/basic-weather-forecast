package com.krungsri.basicweatherforecast.data.api

const val HTTP_TIMEOUT = 60L

// Header
const val HTTP_HEADER_CONTENT_TYPE = "accept"

// Header value
const val HTTP_HEADER_VALUE_CONTENT_TYPE = "application/json"

// parameter app id
const val APP_ID = "&appid="
