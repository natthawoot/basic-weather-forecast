package com.krungsri.basicweatherforecast.data.api.base

import com.krungsri.basicweatherforecast.BuildConfig
import com.krungsri.basicweatherforecast.data.api.HTTP_TIMEOUT
import com.krungsri.basicweatherforecast.data.api.interceptor.AuthInterceptor
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

class BaseOkHttpClientBuilder(private val authInterceptor: AuthInterceptor) {

    fun build(interceptors: Array<out Interceptor>): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(authInterceptor)
            .apply {
                interceptors.forEach {
                    addInterceptor(it)
                }
            }
            .apply {
                if (BuildConfig.DEBUG) {
                    val logging = HttpLoggingInterceptor()
                    logging.level = HttpLoggingInterceptor.Level.BODY
                    addInterceptor(logging)
                }
            }
            .build()
    }
}