package com.krungsri.basicweatherforecast.data.api.entity.response

import com.google.gson.annotations.SerializedName

class ErrorResponse {

    @SerializedName("code")
    var code: String = ""

    @SerializedName("message")
    var message: String = ""
}