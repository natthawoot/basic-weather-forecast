package com.krungsri.basicweatherforecast.data.api.entity.response

import com.google.gson.annotations.SerializedName

class ForecastResponse {

    @SerializedName("current")
    var currentForecast: CurrentForecast? = null

    @SerializedName("hourly")
    var hourly: List<HourForecast> = listOf()

}

class CurrentForecast {

    @SerializedName("temp")
    var temp: Double = 0.0

    @SerializedName("humidity")
    var humidity: Int = 0

    @SerializedName("feels_like")
    var feelsLike: Double = 0.0

    @SerializedName("weather")
    var weather: List<Weather> = listOf()

}

class HourForecast {

    @SerializedName("dt")
    var dateTime: Int = 0

    @SerializedName("temp")
    var temp: Double = 0.0

    @SerializedName("humidity")
    var humidity: Int = 0

    @SerializedName("feels_like")
    var feelsLike: Double = 0.0

    @SerializedName("weather")
    var weather: List<Weather> = listOf()

}