package com.krungsri.basicweatherforecast.data.api.entity.response

import com.google.gson.annotations.SerializedName

class CurrentWeatherResponse {

    @SerializedName("coord")
    var coordinate: Coordinate? = null

    @SerializedName("weather")
    var weather: List<Weather> = listOf()

    @SerializedName("main")
    var main: Main? = null

    @SerializedName("dt")
    var dateTime: Int = 0

    @SerializedName("name")
    var name: String = ""

    @SerializedName("sys")
    var sys: SystemWeather? = null
}

class Coordinate {

    @SerializedName("lat")
    var lat: Double = 0.0

    @SerializedName("lon")
    var lon: Double = 0.0
}

class Main {

    @SerializedName("temp")
    var temp: Double = 0.0

    @SerializedName("feels_like")
    var feelsLike: Double = 0.0

    @SerializedName("temp_min")
    var tempMin: Double = 0.0

    @SerializedName("temp_max")
    var tempMax: Double = 0.0

    @SerializedName("humidity")
    var humidity: Int = 0
}

class SystemWeather {

    @SerializedName("country")
    var country: String = ""

    @SerializedName("sunrise")
    var sunrise: Int = 0

    @SerializedName("sunset")
    var sunset: Int = 0
}