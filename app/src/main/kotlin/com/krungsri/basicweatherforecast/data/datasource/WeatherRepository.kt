package com.krungsri.basicweatherforecast.data.datasource

import com.krungsri.basicweatherforecast.data.api.WeatherApi
import com.krungsri.basicweatherforecast.data.api.entity.response.CurrentWeatherResponse
import com.krungsri.basicweatherforecast.data.api.entity.response.ForecastResponse
import io.reactivex.Single

interface WeatherRepository {

    fun getCurrentWeather(city: String, unit: String): Single<CurrentWeatherResponse>

    fun getForecast(lat: Double, lon: Double, unit: String): Single<ForecastResponse>
}

class WeatherRepositoryImpl(private val weatherApi: WeatherApi): WeatherRepository {

    override fun getCurrentWeather(city: String, unit: String): Single<CurrentWeatherResponse> {
        return weatherApi.getCurrentWeather(city, unit)
    }

    override fun getForecast(lat: Double, lon: Double, unit: String): Single<ForecastResponse> {
        return weatherApi.getForecast(lat, lon, unit)
    }

}