package com.krungsri.basicweatherforecast.data.entity

data class WeatherForecastData(
    var headerWeatherForecastData: MutableList<HeaderWeatherForecastData>
)

data class HeaderWeatherForecastData(var icon: String, var temp: Double,
                                     var place: String, var contentWeatherList: MutableList<ContentWeatherForecastData>)

data class ContentWeatherForecastData(
    var icon: String, var temp: Double, var dateTime: Int,
    var humidity: Int, var feelsLike: Double
)