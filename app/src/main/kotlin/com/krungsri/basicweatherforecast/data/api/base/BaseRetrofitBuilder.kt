package com.krungsri.basicweatherforecast.data.api.base

import com.krungsri.basicweatherforecast.API_BASE_URL
import okhttp3.Interceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit

class BaseRetrofitBuilder(
    val baseOkHttpClientBuilder: BaseOkHttpClientBuilder,
    val callAdapterFactory: CallAdapter.Factory,
    val converterFactory: Converter.Factory
) {

    inline fun <reified T> build(vararg interceptor: Interceptor): T {
        return Retrofit.Builder()
            .addCallAdapterFactory(callAdapterFactory)
            .addConverterFactory(converterFactory)
            .client(baseOkHttpClientBuilder.build(interceptor))
            .baseUrl(API_BASE_URL)
            .build()
            .create(T::class.java)
    }
}