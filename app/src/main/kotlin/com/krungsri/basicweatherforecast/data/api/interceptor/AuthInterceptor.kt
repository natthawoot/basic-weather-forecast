package com.krungsri.basicweatherforecast.data.api.interceptor

import com.krungsri.basicweatherforecast.API_KEY
import com.krungsri.basicweatherforecast.data.api.APP_ID
import com.krungsri.basicweatherforecast.data.api.HTTP_HEADER_CONTENT_TYPE
import com.krungsri.basicweatherforecast.data.api.HTTP_HEADER_VALUE_CONTENT_TYPE
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val url = originalRequest.url.toString()
        val requestBuilder = originalRequest.newBuilder()
        requestBuilder.addHeader(HTTP_HEADER_CONTENT_TYPE, HTTP_HEADER_VALUE_CONTENT_TYPE)
        requestBuilder.url(url = url + APP_ID + API_KEY)
        return chain.proceed(requestBuilder.build())
    }
}