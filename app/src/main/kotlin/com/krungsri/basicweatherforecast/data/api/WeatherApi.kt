package com.krungsri.basicweatherforecast.data.api

import com.krungsri.basicweatherforecast.data.api.entity.response.CurrentWeatherResponse
import com.krungsri.basicweatherforecast.data.api.entity.response.ForecastResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("/data/2.5/weather")
    fun getCurrentWeather(
        @Query("q") city: String,
        @Query("units") unit: String
    ): Single<CurrentWeatherResponse>

    @GET("/data/2.5/onecall")
    fun getForecast(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("units") unit: String
    ): Single<ForecastResponse>
}