package com.krungsri.basicweatherforecast.application

import android.app.Application
import com.krungsri.basicweatherforecast.di.networkModule
import com.krungsri.basicweatherforecast.di.weatherModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WeatherApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@WeatherApplication)
            modules(networkModule, weatherModule)
        }
    }
}