package com.krungsri.basicweatherforecast.constants

const val CELSIUS = "metric"
const val FAHRENHEIT = "imperial"

const val HOUR_24 = 24
