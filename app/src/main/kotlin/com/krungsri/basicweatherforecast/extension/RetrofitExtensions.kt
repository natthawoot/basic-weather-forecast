package com.krungsri.basicweatherforecast.extension

import com.google.gson.Gson
import retrofit2.HttpException

inline fun <reified T> HttpException.toObject(): T? {
    response()?.errorBody()?.let {
        val json = it.string()
        return try {
            Gson().fromJson(json, T::class.java)
        } catch (e: Exception) {
            null
        }
    }
    return null
}

