package com.krungsri.basicweatherforecast.extension

import java.text.SimpleDateFormat
import java.util.*

const val FORMAT_DATE_TIME = "dd/MM/yyyy HH:mm"
const val THAILAND_TIMEZONE_ID = "GMT+7"


fun Date.format(format: String = FORMAT_DATE_TIME, timeZone: String = THAILAND_TIMEZONE_ID): String {
    val sdf = SimpleDateFormat(format, Locale.getDefault())
    sdf.timeZone = TimeZone.getTimeZone(timeZone)
    return sdf.format(this)
}

fun Long.toDate(): Date {
    val unix = this * 1000
    return Date(unix)
}
