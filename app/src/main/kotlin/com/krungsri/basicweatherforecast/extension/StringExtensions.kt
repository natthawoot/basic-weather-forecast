package com.krungsri.basicweatherforecast.extension

import com.krungsri.basicweatherforecast.TYPE_IMAGE
import com.krungsri.basicweatherforecast.URL_IMAGE

const val EXPRESSION_CHARACTER_ENG_ALPHABET = "[a-zA-Z]{1,50}"

fun String.getUrlImage(): String {
    return "${URL_IMAGE}${this}${TYPE_IMAGE}"
}

fun String.isCharacterEngAlphabet(): Boolean {
    return this.matches(EXPRESSION_CHARACTER_ENG_ALPHABET.toRegex())
}