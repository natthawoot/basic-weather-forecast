package com.krungsri.basicweatherforecast.extension

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.krungsri.basicweatherforecast.application.GlideApp


fun ImageView.loadImage(url: String?) {
    GlideApp.with(context).load(url)
            .into(this)
}


fun ImageView.loadImage(url: String?, @DrawableRes placeHolder: Int) {
    GlideApp.with(context).load(url)
        .error(placeHolder)
        .placeholder(placeHolder)
        .fallback(placeHolder)
        .into(this)
}