package com.krungsri.basicweatherforecast.state

data class LoadingState private constructor(val status: Status, val message: String? = null) {
    companion object {
        val SUCCESS = LoadingState(
            Status.SUCCESS
        )
        val LOADING = LoadingState(
            Status.LOADING
        )
        fun error(message: String?) =
            LoadingState(
                Status.FAILED,
                message
            )
    }

    enum class Status {
        LOADING,
        SUCCESS,
        FAILED
    }
}