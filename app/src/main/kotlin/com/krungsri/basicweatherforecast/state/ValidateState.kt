package com.krungsri.basicweatherforecast.state

data class ValidateState private constructor(val status: ValidateStatus) {
    companion object {
        val SUCCESS = ValidateState(ValidateStatus.SUCCESS)
        val FAILED = ValidateState(ValidateStatus.FAILED)
    }

    enum class ValidateStatus {
        SUCCESS,
        FAILED
    }
}